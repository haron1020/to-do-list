//
//  AYNMainTableViewCell.h
//  To-Do List
//
//  Created by Andrey Nazarov on 26/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

@import UIKit;

@class AYNItem;

@interface AYNMainTableViewCell : UITableViewCell

@property (strong, nonatomic) AYNItem *item;

- (void)changeStatusAnimated;

@end
