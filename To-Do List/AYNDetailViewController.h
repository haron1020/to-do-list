//
//  AYNDetailViewController.h
//  To-Do List
//
//  Created by Andrey Nazarov on 26/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "AYNBaseViewController.h"

@class AYNItem;

@interface AYNDetailViewController : AYNBaseViewController

@property (strong, nonatomic) AYNItem *item;

@end
