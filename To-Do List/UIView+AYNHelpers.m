//
//  UIView+AYNHelpers.m
//  To-Do List
//
//  Created by Andrey Nazarov on 29/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "UIView+AYNHelpers.h"

#import "UIColor+AYNHelpers.h"

@implementation UIView (AYNHelpers)

- (void)ayn_makeFrameWithRoundCorners {
    self.layer.cornerRadius = 5.0;
    self.clipsToBounds = YES;
    self.layer.borderWidth = 1.0 / [UIScreen mainScreen].scale;
    self.layer.borderColor = [UIColor ayn_placeholderTextColor].CGColor;
}

@end
