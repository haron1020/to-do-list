//
//  AYNItem.m
//  To-Do List
//
//  Created by Andrey Nazarov on 26/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "AYNItem.h"

static NSString *AYNStatusStringWithStatus(AYNItemStatus status) {
    switch (status) {
        case AYNItemStatusNew:
            return @"New";
  
        case AYNItemStatusCompleted:
            return @"Complete";
            
        case AYNItemStatusExpired:
            return @"Expired";
    }

    return nil;
}

@implementation AYNItem

@dynamic title;
@dynamic detail;
@dynamic dueDate;
@dynamic status;

- (void)makeStatusOpposite {
    switch (self.status.integerValue) {
        case AYNItemStatusNew:
            self.status = @(AYNItemStatusCompleted);
            break;
        case AYNItemStatusCompleted:
            self.status = @(AYNItemStatusNew);
            break;
        case AYNItemStatusExpired:
            break;
    }
}

- (NSString *)stringValue {
    NSString *formattedDate = [NSDateFormatter localizedStringFromDate:self.dueDate dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterShortStyle];
    
    return [NSString stringWithFormat:@"%@: %@ until %@. %@", self.title, self.detail, formattedDate, AYNStatusStringWithStatus(self.status.integerValue)];
}

@end
