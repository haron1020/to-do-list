//
//  main.m
//  To-Do List
//
//  Created by Andrey Nazarov on 22/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

@import UIKit;
#import "AYNAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AYNAppDelegate class]));
    }
}
