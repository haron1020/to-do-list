//
//  AYNMainViewController.m
//  To-Do List
//
//  Created by Andrey Nazarov on 26/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "AYNMainViewController.h"

#import "UIColor+AYNHelpers.h"

#import "AYNMainTableViewCell.h"
#import "AYNItem.h"
#import "AYNStatusView.h"
#import "AYNItemAdditionViewController.h"
#import "AYNDetailViewController.h"
#import "AYNAppManager.h"

static NSString * const kAYNMainViewControllerItemDetailsSegueIdentifier = @"AYNMainViewControllerItemDetailsSegueIdentifier";

@interface AYNMainViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeGestureRecognizer;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) AYNItem *itemToPass;

@end

@implementation AYNMainViewController

#pragma mark - View Controller Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self markExpiredTasks];
}

#pragma mark - Private

- (void)handleShareButton {
    NSMutableArray<NSString *> *activityItems = [NSMutableArray array];
    [activityItems addObject:@"To-Do: "];
    
    [self.fetchedResultsController.fetchedObjects enumerateObjectsUsingBlock:^(AYNItem   * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [activityItems addObject:[NSString stringWithFormat:@"- %@", obj.stringValue]];
    }];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems.copy applicationActivities:nil];
    
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (IBAction)handleSwipeRight:(UISwipeGestureRecognizer *)sender {
    [self swipeRight];
}

- (void)swipeRight {
    CGPoint location = [self.swipeGestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    if (indexPath) {
        [self changeStatusForRowAtIndexPathAnimated:indexPath];
    }
}

- (void)changeStatusForRowAtIndexPathAnimated:(NSIndexPath *)indexPath {
    [[self.tableView cellForRowAtIndexPath:indexPath] changeStatusAnimated];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kAYNMainViewControllerItemDetailsSegueIdentifier]) {
        AYNDetailViewController *destinationViewController = segue.destinationViewController;
        destinationViewController.item = self.itemToPass;
    }
}

- (void)markExpiredTasks {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.dueDate < %@ AND SELF.status == %@", [NSDate date], @(AYNItemStatusNew)];
    NSArray<AYNItem *> *filtered = [self.fetchedResultsController.fetchedObjects filteredArrayUsingPredicate:predicate];
    
    [filtered enumerateObjectsUsingBlock:^(AYNItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.status = @(AYNItemStatusExpired);
    }];
}

#pragma mark - Actions

- (IBAction)shareButtonTapped:(UIBarButtonItem *)sender {
    [self handleShareButton];
}

#pragma mark - Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.itemToPass = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:kAYNMainViewControllerItemDetailsSegueIdentifier sender:self];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Table View Data Source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AYNMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AYNMainTableViewCell class]) forIndexPath:indexPath];
    
    cell.item = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [[AYNAppManager sharedInstance].managedObjectContext deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController == nil) {
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([AYNItem class])];
        request.sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:NSStringFromSelector(@selector(dueDate)) ascending:YES]];
        _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[AYNAppManager sharedInstance].managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        
        NSError *error = nil;
        if (![_fetchedResultsController performFetch:&error]) {
            NSLog(@"Error while setting fetchedResultsController! %@ %@", error.localizedDescription, error.userInfo);
        }
        _fetchedResultsController.delegate = self;
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
            [self.tableView moveSection:indexPath.section toSection:newIndexPath.section];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

@end
