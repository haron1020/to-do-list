//
//  AYNStatusView.h
//  To-Do List
//
//  Created by Andrey Nazarov on 26/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

@import UIKit;

#import "AYNItem.h"

@interface AYNStatusView : UIView

@property (assign, nonatomic) AYNItemStatus status;

@end
