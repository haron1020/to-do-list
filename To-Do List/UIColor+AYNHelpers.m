//
//  UIColor+AYNHelpers.m
//  To-Do List
//
//  Created by Andrey Nazarov on 29/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "UIColor+AYNHelpers.h"

#import "AYNMainTableViewCell.h"
#import "AYNItem.h"

@implementation UIColor (AYNHelpers)

+ (UIColor *)ayn_placeholderTextColor {
    return [UIColor colorWithWhite:0.77 alpha:1.0];
}

+ (UIColor *)ayn_newTaskColor {
    return [UIColor colorWithRed:1.0 green:1.0 blue:0.0 alpha:0.3];
}

+ (UIColor *)ayn_completedTaskColor {
    return [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.3];
}

+ (UIColor *)ayn_expiredTaskColor {
    return [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.3];
}

+ (UIColor *)ayn_animationColorForStatus:(AYNItemStatus)status {
    switch (status) {
        case AYNItemStatusCompleted:
            return [UIColor ayn_newTaskColor];
        case AYNItemStatusNew:
            return [UIColor ayn_completedTaskColor];
        case AYNItemStatusExpired:
            return [UIColor ayn_expiredTaskColor];
    }
    return [UIColor clearColor];
}

@end
