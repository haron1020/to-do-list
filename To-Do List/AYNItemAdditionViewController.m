//
//  AYNItemAdditionViewController.m
//  To-Do List
//
//  Created by Andrey Nazarov on 26/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "AYNItemAdditionViewController.h"

#import "UIColor+AYNHelpers.h"
#import "UIView+AYNHelpers.h"

#import "AYNItem.h"
#import "AYNAppManager.h"

static NSUInteger kAYNMaxStringLength = 40;

@interface AYNItemAdditionViewController () <UITextViewDelegate, UITextFieldDelegate, UINavigationBarDelegate>

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
@property (weak, nonatomic) IBOutlet UILabel *detailTextViewPlaceholderLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBarButtonItem;

@end

@implementation AYNItemAdditionViewController

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
}

#pragma mark - Private

- (IBAction)cancelButtonTapped:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneButtonTapped:(UIBarButtonItem *)sender {
    [self addNewItem];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setupUI {
    self.detailLabel.alpha = 0.0;
    self.titleLabel.alpha = 0.0;
    
    self.detailTextViewPlaceholderLabel.textColor = [UIColor ayn_placeholderTextColor];
    [self.detailTextView ayn_makeFrameWithRoundCorners];
    
    self.datePicker.minimumDate = [NSDate date];
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)addNewItem {
    [[AYNAppManager sharedInstance].managedObjectContext performBlock:^{
        AYNItem *item = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([AYNItem class]) inManagedObjectContext:[AYNAppManager sharedInstance].managedObjectContext];
        
        item.title = self.titleTextField.text;
        item.detail = self.detailTextView.text;
        item.dueDate = self.datePicker.date;
        item.status = @(AYNItemStatusNew);
    }];
}

#pragma mark - UI Text View Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [UIView animateWithDuration:0.3 animations:^{
        self.detailLabel.alpha = 1.0;
    }];
    
    self.detailTextViewPlaceholderLabel.hidden = [textView hasText] ? YES : NO;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (![textView hasText]) {
        [UIView animateWithDuration:0.3 animations:^{
            self.detailLabel.alpha = 0.0;
        }];
    }
    
    [textView resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView {
    self.detailTextViewPlaceholderLabel.hidden = [textView hasText] ? YES : NO;
}

#pragma mark - UI Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (![textField hasText]) {
        [UIView animateWithDuration:0.3 animations:^{
            self.titleLabel.alpha = 1.0;
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (![textField hasText]) {
        [UIView animateWithDuration:0.3 animations:^{
            self.titleLabel.alpha = 0.0;
        }];
    }
    
    [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString * newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.doneBarButtonItem.enabled = [newString isEqualToString:@""] ? NO : YES;
    
    return newString.length <= kAYNMaxStringLength;
}

#pragma mark - Navigation Bar Delegate

- (UIBarPosition)positionForBar:(UINavigationBar *)bar {
    return UIBarPositionTopAttached;
}

#pragma mark - Responder

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.titleTextField endEditing:YES];
    [self.detailTextView endEditing:YES];
}

@end
