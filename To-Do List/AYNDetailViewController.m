//
//  AYNDetailViewController.m
//  To-Do List
//
//  Created by Andrey Nazarov on 26/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "AYNDetailViewController.h"

#import "NSString+AYNHelpers.h"
#import "UIColor+AYNHelpers.h"
#import "UIView+AYNHelpers.h"

#import "AYNItem.h"

@interface AYNDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation AYNDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
}

- (void)setupUI {
    self.titleLabel.text = self.item.title;
    self.detailTextView.text = self.item.detail;
    self.dateLabel.text = [NSString ayn_stringWithDate:self.item.dueDate];
    
    [self.detailTextView scrollRangeToVisible:NSMakeRange(0, 0)];
    
    [self.detailTextView ayn_makeFrameWithRoundCorners];
    [self.titleLabel ayn_makeFrameWithRoundCorners];
    [self.dateLabel ayn_makeFrameWithRoundCorners];
}

@end
