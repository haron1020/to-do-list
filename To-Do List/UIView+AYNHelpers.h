//
//  UIView+AYNHelpers.h
//  To-Do List
//
//  Created by Andrey Nazarov on 29/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

@import UIKit;

@interface UIView (AYNHelpers)

- (void)ayn_makeFrameWithRoundCorners;

@end
