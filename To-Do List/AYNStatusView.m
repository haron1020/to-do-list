//
//  AYNStatusView.m
//  To-Do List
//
//  Created by Andrey Nazarov on 26/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "AYNStatusView.h"

static CGFloat kAYNStatusViewLineWidth = 3.0;
static CGLineCap kAYNStatusViewLineCapStyle = kCGLineCapRound;

@implementation AYNStatusView

- (void)setStatus:(AYNItemStatus)status {
    _status = status;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    switch (self.status) {
        case AYNItemStatusNew:
            return [self drawCircle];
        case AYNItemStatusExpired:
            return [self drawCross];
        case AYNItemStatusCompleted:
            return [self drawCheckMark];
    }
}

- (void)drawCircle {
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectInset(self.bounds, CGRectGetMidX(self.bounds) / 2.0, CGRectGetMidY(self.bounds) / 2.0)];
    
    path.lineWidth = kAYNStatusViewLineWidth;
    path.lineCapStyle = kAYNStatusViewLineCapStyle;
    [[UIColor yellowColor] setStroke];
    
    [path stroke];
}

- (void)drawCross {
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGRect pathBounds = CGRectInset(self.bounds, CGRectGetMidX(self.bounds) / 2.0, CGRectGetMidY(self.bounds) / 2.0);
    
    [path moveToPoint:CGPointMake(CGRectGetMinX(pathBounds), CGRectGetMinY(pathBounds))];
    [path addLineToPoint:CGPointMake(CGRectGetMaxX(pathBounds), CGRectGetMaxY(pathBounds))];
    [path moveToPoint:CGPointMake(CGRectGetMaxX(pathBounds), CGRectGetMinY(pathBounds))];
    [path addLineToPoint:CGPointMake(CGRectGetMinX(pathBounds), CGRectGetMaxY(pathBounds))];
    
    [[UIColor redColor] setStroke];
    path.lineCapStyle = kAYNStatusViewLineCapStyle;
    path.lineWidth = kAYNStatusViewLineWidth;
    
    [path stroke];
}

- (void)drawCheckMark {
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGRect pathBounds = CGRectInset(self.bounds, CGRectGetMidX(self.bounds) / 2.0, CGRectGetMidY(self.bounds) / 2.0);
    
    [path moveToPoint:CGPointMake(CGRectGetMaxX(pathBounds), CGRectGetMinY(pathBounds))];
    [path addLineToPoint:CGPointMake(CGRectGetMaxX(pathBounds) / 2.0, CGRectGetMaxY(pathBounds))];
    [path addLineToPoint:CGPointMake(CGRectGetMinY(pathBounds), CGRectGetMidY(pathBounds))];
    
    [[UIColor greenColor] setStroke];
    path.lineCapStyle = kAYNStatusViewLineCapStyle;
    path.lineWidth = kAYNStatusViewLineWidth;
    
    [path stroke];
}

@end
