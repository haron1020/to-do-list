//
//  AYNAppDelegate.m
//  To-Do List
//
//  Created by Andrey Nazarov on 22/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "AYNAppDelegate.h"

#import "AYNAppManager.h"

@interface AYNAppDelegate ()

@end

@implementation AYNAppDelegate

#pragma mark - Application Delegate

- (void)applicationWillResignActive:(UIApplication *)application {
    [[AYNAppManager sharedInstance] saveContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[AYNAppManager sharedInstance] saveContext];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[AYNAppManager sharedInstance] saveContext];
}

@end
