//
//  AYNItem.h
//  To-Do List
//
//  Created by Andrey Nazarov on 26/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

@import Foundation;
@import CoreData;

typedef NS_ENUM (NSUInteger, AYNItemStatus) {
    AYNItemStatusNew,
    AYNItemStatusExpired,
    AYNItemStatusCompleted,
};

@interface AYNItem : NSManagedObject

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *detail;
@property (copy, nonatomic) NSDate *dueDate;
@property (copy, nonatomic) NSNumber *status;

- (void)makeStatusOpposite;

- (NSString *)stringValue;

@end
