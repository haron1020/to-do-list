//
//  NSString+AYNHelpers.m
//  To-Do List
//
//  Created by Andrey Nazarov on 29/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "NSString+AYNHelpers.h"

@implementation NSString (AYNHelpers)

+ (NSString *)ayn_stringWithDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat =  @"dd-MM-yyyy HH:mm";
    NSString *result = [dateFormatter stringFromDate:date];
    
    return result;
}

@end
