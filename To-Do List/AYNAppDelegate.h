//
//  AYNAppDelegate.h
//  To-Do List
//
//  Created by Andrey Nazarov on 22/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

@import UIKit;

@interface AYNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

