//
//  AYNAppManager.h
//  To-Do List
//
//  Created by Andrey Nazarov on 28/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

@import Foundation;
@import CoreData;

@interface AYNAppManager : NSObject

@property (strong, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic, readonly) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+ (AYNAppManager *)sharedInstance;

@end
