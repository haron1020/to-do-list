//
//  UIColor+AYNHelpers.h
//  To-Do List
//
//  Created by Andrey Nazarov on 29/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

@import UIKit;
#import "AYNItem.h"

@interface UIColor (AYNHelpers)

+ (UIColor *)ayn_placeholderTextColor;
+ (UIColor *)ayn_newTaskColor;
+ (UIColor *)ayn_completedTaskColor;
+ (UIColor *)ayn_expiredTaskColor;
+ (UIColor *)ayn_animationColorForStatus:(AYNItemStatus)status;

@end
