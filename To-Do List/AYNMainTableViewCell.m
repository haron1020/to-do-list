//
//  AYNMainTableViewCell.m
//  To-Do List
//
//  Created by Andrey Nazarov on 26/06/16.
//  Copyright © 2016 Andrey Nazarov. All rights reserved.
//

#import "AYNMainTableViewCell.h"

#import "UIColor+AYNHelpers.h"

#import "AYNStatusView.h"

@interface AYNMainTableViewCell ()

@property (weak, nonatomic) IBOutlet AYNStatusView *statusView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;

@end

@implementation AYNMainTableViewCell

- (void)setItem:(AYNItem *)item {
    _item = item;
    
    self.titleLabel.text = item.title;
    self.detailLabel.text = item.detail;
    self.statusView.status = item.status.integerValue;
}

- (void)changeStatusAnimated {
    self.backgroundColor = [UIColor ayn_animationColorForStatus:self.item.status.integerValue];
    self.leadingConstraint.constant = 20;
    [self layoutIfNeeded];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.backgroundColor = [self.backgroundColor colorWithAlphaComponent:0.0];
        self.leadingConstraint.constant = 0;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.item makeStatusOpposite];
    }];
}

@end
